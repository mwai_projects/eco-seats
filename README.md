# ECO-seats
A micro-website with transitions, animations and 3D models

## Demo
[Live](https://eco-seats.mwayianyula.com/) example

## Libraries

The Libraries were mainly used in this single page animated site.

- [ ] [Tailwindcss](https://tailwindcss.com/) with [DaisyUi](https://daisyui.com/)
- [ ] [GSAP](https://greensock.com/gsap/)
- [ ] [Model-Viewer](https://modelviewer.dev/)
