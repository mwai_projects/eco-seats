module.exports = {
    content: ["index.html","./src/**/*.{js,ts,jsx,tsx}"],
    theme: {
      extend: {
        screens: {
          'xs': '480px',
          'ml': '960px',
          'xxl': '1600px',
        },
        animation: {
          'spin-slow': 'spin 3s linear infinite',
          'bounce-slow': 'bounce 3s infinite',
        },
        colors: {
          base: '#555',
          limeish: '#d9ccaa',
          facebook: '#4267B2',
          instagram: '#C13584',
          twitter: '#1DA1F2',
          youtube: '#FF0000',
          linkedin: '#0077b5',
          whatsapp: '#25D366',
          black: '#000',
        },
        margin: {
          '2.5vw': '2.5vw',
        },
        padding: {
          '2.5vw': '2.5vw',
        },
        lineHeight: {
          'extra-loose': '2.5',
          '12': '3.5rem',
        },
        maxHeight: {
          '1400': '1400px',
        },
        fontFamily: {
          'sans': ['Roboto\ Condensed', 'sans-serif'],
          'serif': ['Playfair\ Display', 'serif'],
        },
        aspectRatio: {
          '3/2': '3 / 2',
          '2/3': '2 / 2.81',
          '4/3': '4 / 3',
          '2/1': '2 / 1',
        },
        zIndex: {
          '1': '1',
        },
        rotate: {
          '270': '270deg',
        }
      },
    },
    daisyui: {
      themes: [
        {
          mytheme: {          
          
            "primary": "#7da6e0",
          
            "secondary": "#e8dc3e",
                     
            "accent": "#68ce1e",
                     
            "neutral": "#1A1B28",
                     
            "base-100": "#EEF3F7",
                     
            "info": "#86D7EA",
                     
            "success": "#156F53",
                     
            "warning": "#FBB150",
                     
            "error": "#EF3969",
          },
        },
      ],
    },
    plugins: [
      require('@tailwindcss/typography'),
      require("daisyui")
    ],
  }